<h1> zKinectV2osc2022 </h1>
This Windows applicaiton provides a simple way to access the Kinects skeletal information via OSC messagin.


The app is built on the project: https://github.com/microcosm/KinectV2-OSC and provides the extended functionality described below.


<h2> Additional and modified messages </h2>

Global status message once per second:

```sh
Address: /kinect2/status
Values: - float:  depthClipMeters
```

User Tracking Status message

```sh
Address: /kinect2/{bodyId: 0 - 5 }/tracking
values:	- trackingSatus (1 or 0)
```

Joints Messages are in LOCAL coordinates, relative to the SpineMid joint, and position values are in meters.

Modified message protocol for joints:

```sh
Address: /kinect2/{bodyId: 0 - 5 }/joint/{jointId}
Values: - float:  positionX
        - float:  positionY
        - float:  positionZ		 
```
The complete jointId list can be found here:  https://docs.microsoft.com/en-us/previous-versions/windows/kinect/dn758662(v=ieb.10)


Skeletal pose frame synchronization message (output at each frame boundary)

```sh
Address: /kinect2/{bodyId: 0 - 5 }/skelFrame
```

Message protocol for hands:

```sh
Address: /kinect2/{bodyId: 0 - 5 }/hands/Left or  ..../Right 
Value - symbol:  Open, CLosed, Lasso, or NotTracked
```


The three skeleton global pose messages, below, are based on the skeleton's "SpineMid" joint.
note positionZ value is the distance from Kinect: ranging from 0 to -4.5

User global position
```sh
Address: /kinect2/{bodyId: 0 - 5 }/position
Values: - float:  positionX
        - float:  positionY
        - float:  positionZ
```   
 
User global orientation (in degrees)
```sh
Address: /kinect2/{bodyId: 0 - 5 }/eulerDegs
Values: - float:  pitch 
        - float:  yaw
        - float:  roll
```

User global 6DoF (position and quaternian)
```sh
Address: /kinect2/{bodyId: 0 - 5 }/6dof
Values: - float:  positionX
        - float:  positionY
        - float:  positionZ		 
        - float:  quaternianX
        - float:  quaternianY
        - float:  quaternianZ
        - float:  quaternianW
        
 Address: /kinect2/{bodyId: 0 - 5 }/lean        
        - float: leanFrontRear
        - float: leanLeftRight
```



<h2> Useful command line argument flags </h2>

            -p , --port, DefaultValue = 64321

            -i, --ip, DefaultValue = your local network broadcast (e.g. 192.168.0.255)

            -r, --oscUpdateRateMs, DefaultValue = 50 ms

            -d,  --depthClip", DefaultValue = 4.5 meters   (maximum tracking distance from the kinect)

            -j, --jointTX, DefaultValue = 1 (enable/disable osc joint messages)

            -m, --maxUsers, DefaultValue = 1 user  (maximum number of users tracked)
			


<h3> Other </h3>
        UDB broadcast seems to fail over Wifi
				
			
