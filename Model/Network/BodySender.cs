﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Numerics;
using Microsoft.Kinect;
using Rug.Osc;

namespace zKinectV2OSC.Model.Network
{
    public class BodySender
    {

        private MessageBuilder messageBuilder;
        private List<OscSender> oscSenders;
        private List<IPAddress> ipAddresses;
        private OscMessage message;
        private string port;
        private string status;
        private Enum[] LHstate = new Enum[6];
        private Enum[] RHstate = new Enum[6];
        public static bool[] trackingStates = new bool[6];

        public bool jointTX = true;
        public float depthClip = 4.5f;

        static float rad2Deg = 57.295827908797774f;


        public BodySender(string delimitedIpAddresses, string port)
        {
            this.status = "";
            this.ipAddresses = this.Parse(delimitedIpAddresses);
            this.oscSenders = new List<OscSender>();
            this.port = port;
            this.messageBuilder = new MessageBuilder();
            this.TryConnect();

            for (int i = 0; i < 6; i++)
            {
               RHstate[i] = LHstate[i] = HandState.NotTracked;
            }
        }



        private void TryConnect()
        {
            foreach (var ipAddress in this.ipAddresses)
            {
                try
                {
                    int localPort = 0; // this way, the local port will not be bound
                    var oscSender = new OscSender(ipAddress, localPort,  int.Parse(this.port));
                    oscSender.Connect();
                    this.oscSenders.Add(oscSender);
                    this.status += "OSC connection established on\nIP: " + ipAddress + "\tPort: " + port + "\n";
                }
                catch (Exception e)
                {
                    this.status += "Unable to make OSC connection on\nIP: " + ipAddress + "\tPort: " + port + "\n";
                    Console.WriteLine("Exception on OSC connection...");
                    Console.WriteLine(e.StackTrace);
                }
            }

        }

        public void sendStatus(float distanceClip)
        {
            message = messageBuilder.BuildStatusMessage(distanceClip);
            this.Broadcast(message);
        }

        public void Send(Body[] bodies, int bodycount)
        {
            int index = 0;
            int bodyID = 0;
            bool[] isTrackedStates = new bool[6];

            foreach (Body body in bodies)
            {
                if (body != null && body.IsTracked && index < bodycount)
                {
                    if (body.Joints[JointType.SpineMid].Position.Z > depthClip)
                        isTrackedStates[index] = false;
                    else
                        isTrackedStates[index] = true;
                    this.Send(body, bodyID++);
                    index++;
                }
            }

            for (index = 0; index < 6 && index < bodycount; index++ )
            {
                 if (isTrackedStates[index] != trackingStates[index])
                {
                    trackingStates[index] = isTrackedStates[index];
                    message = messageBuilder.BuildBodyTrackingStatusMessage(index, trackingStates[index]);
                    this.Broadcast(message);
                }
            }

        }

        public string GetStatusText()
        {
            return this.status;
        }

        private void Send(Body body, int bodiesIndex)
        {
            bool newSkelData = false;

            if (body.Joints[JointType.SpineMid].Position.Z > depthClip) return;

            // use SpineMid as body origin
            System.Numerics.Vector3 bodyPosition = new System.Numerics.Vector3(
                body.Joints[JointType.SpineMid].Position.X,
                body.Joints[JointType.SpineMid].Position.Y,
                body.Joints[JointType.SpineMid].Position.Z);

            // Could add an out-of-bounds message here when body parts are out if kinect's view 
            //if (body.ClippedEdges.HasFlag(FrameEdges.Bottom))

            if (jointTX)
            {
                System.Numerics.Vector3 relPos = new System.Numerics.Vector3();
                foreach (var joint in body.Joints)
                {
                    if (joint.Value.TrackingState.Equals(TrackingState.Tracked))
                    {
                        relPos.X = joint.Value.Position.X;
                        relPos.Y = joint.Value.Position.Y;
                        relPos.Z = joint.Value.Position.Z;

                        relPos -= bodyPosition;  // make joint relative to body
                        relPos.Z = -relPos.Z;  // negate this puppy to unMirror

                        // for wrists use 6DoF
                    /*    if (false) //( String.Equals(joint.Value.JointType, JointType.ElbowLeft)  || String.Equals(joint.Value.JointType, JointType.HandRight))
                        {
                            // process only wrists that are well tracked
                            if (true)
                                //(String.Equals(joint.Value.JointType, JointType.WristLeft) && body.HandLeftConfidence == TrackingConfidence.High)
                               // ||
                               // (String.Equals(joint.Value.JointType, JointType.WristRight) && body.HandRightConfidence == TrackingConfidence.High))
                            {

                                Quaternion jQuat = new Quaternion(body.JointOrientations[joint.Value.JointType].Orientation.X,
                                                body.JointOrientations[joint.Value.JointType].Orientation.Y,
                                                body.JointOrientations[joint.Value.JointType].Orientation.Z,
                                                body.JointOrientations[joint.Value.JointType].Orientation.W);
                                Quaternion invXqut = Quaternion.CreateFromYawPitchRoll((float)Math.PI, 0, 0); // create inverse X axix quat
                                Quaternion jQuatInv = Quaternion.Multiply(jQuat, invXqut);   // flip it around so its not mirrored

                                System.Numerics.Vector4 outRot = new System.Numerics.Vector4(-1, -1, 1, 1);  // these are the needed inverseScalers so it works well with Unity

                                outRot *= new System.Numerics.Vector4(jQuatInv.X, jQuatInv.Y, jQuatInv.Z, jQuatInv.W);  // oriented for Unity3D
                                //Vector3 eulers = outRot.

                                message = messageBuilder.Build6DjointMessage(body, joint.Value, relPos, outRot, bodiesIndex);
                            }
                            // otherwise do nothing
                        }
                        else // 3DoF
                        {
                            message = messageBuilder.BuildJointMessage(body, joint.Value, relPos, bodiesIndex);
                        }*/
                        message = messageBuilder.BuildJointMessage(body, joint.Value, relPos, bodiesIndex); 
                        this.Broadcast(message);
                        newSkelData = true;
                    }
                }
            }

            if (body.HandLeftConfidence.Equals(TrackingConfidence.High) ||
                body.HandLeftState.Equals(HandState.NotTracked) )
            {
                if ( LHstate[bodiesIndex].Equals(body.HandLeftState) == false ) // update only if state has changed, ignore redundent values
                {
                    LHstate[bodiesIndex] = body.HandLeftState; 
                    message = messageBuilder.BuildHandMessage(body, "Left", body.HandLeftState, bodiesIndex);
                    this.Broadcast(message);
                    //newSkelData = true;
                }
           }

            if (body.HandRightConfidence.Equals(TrackingConfidence.High) ||
                body.HandRightState.Equals(HandState.NotTracked))
            {
                if (RHstate[bodiesIndex].Equals(body.HandRightState) == false) // ignore redundent values
                {
                    RHstate[bodiesIndex] = body.HandRightState;
                    message = messageBuilder.BuildHandMessage(body, "Right", body.HandRightState, bodiesIndex);
                    this.Broadcast(message);
                    //newSkelData = true;
                }
            }


            if (newSkelData)
            {
                message = messageBuilder.BuildSkelFrameMessage(bodiesIndex);
                this.Broadcast(message);
            }

            if (body.LeanTrackingState.Equals(TrackingState.Tracked))
            {
                message = messageBuilder.BuildLeanMessage(body, bodiesIndex);
                this.Broadcast(message);
            }
            // using the code below, we could calculate our own rotation but the built-in one looks pretty good
            // if we have a stable pose calculate global rotation
            /*            if (body.Joints[JointType.SpineBase].TrackingState.Equals(TrackingState.Tracked)
                           && body.Joints[JointType.ShoulderLeft].TrackingState.Equals(TrackingState.Tracked)
                           && body.Joints[JointType.ShoulderRight].TrackingState.Equals(TrackingState.Tracked))
                           */

            // Calculate 6DoF message for global skeleton- using the built-in rotation
            if ( body.Joints[JointType.SpineMid].TrackingState.Equals(TrackingState.Tracked) )
            {
                Vector3 pos = new Vector3(body.Joints[JointType.SpineMid].Position.X, body.Joints[JointType.SpineMid].Position.Y, body.Joints[JointType.SpineMid].Position.Z);

                Quaternion jQuat = new Quaternion(body.JointOrientations[JointType.SpineMid].Orientation.X,
                                             body.JointOrientations[JointType.SpineMid].Orientation.Y,
                                             body.JointOrientations[JointType.SpineMid].Orientation.Z,
                                             body.JointOrientations[JointType.SpineMid].Orientation.W);
                Quaternion invXqut = Quaternion.CreateFromYawPitchRoll( (float)Math.PI,  0,  0 ); // create inverse X axix quat
                Quaternion jQuatInv = Quaternion.Multiply(jQuat, invXqut);   // flip it around so its not mirrored

                System.Numerics.Vector4 outRot = new System.Numerics.Vector4(-1, -1, 1, 1);  // these are the needed inverseScalers so it works well with Unity

                outRot *= new System.Numerics.Vector4(jQuatInv.X, jQuatInv.Y, jQuatInv.Z, jQuatInv.W);  // oriented for Unity3D
                Vector3 angles = ComputeAngles(outRot); // get eulers

                // convert to degrees (-180 to 180
                angles.X *= rad2Deg;
                angles.Y *= rad2Deg;
                angles.Z *= rad2Deg;

                // invert z to describe negative distance from the kinect
                pos.Z *= -1f;

                message = messageBuilder.Build6DOFMessage(outRot, pos,  bodiesIndex);
                this.Broadcast(message);

                message = messageBuilder.BuildPositiontMessage(pos, bodiesIndex);
                this.Broadcast(message);

                message = messageBuilder.BuildRotMessage(angles, bodiesIndex);
                this.Broadcast(message);

            }
        }

        private void Broadcast(OscMessage message)
        {
            foreach (var oscSender in this.oscSenders)
            {
                oscSender.Send(message);
            }
        }

        private void getBodyQuat (Body bod)
        {     
            Vector3 u,  v;
            // enough points to calculate a rotation?
            if (bod.Joints[JointType.SpineBase].TrackingState.Equals(TrackingState.NotTracked)
                || bod.Joints[JointType.ShoulderLeft].TrackingState.Equals(TrackingState.NotTracked)
                || bod.Joints[JointType.ShoulderRight].TrackingState.Equals(TrackingState.NotTracked))
                return;
            // else calculate and send rotation

            u = new Vector3();
            v = new Vector3();



            float m = (float)Math.Sqrt(2 + 2 * System.Numerics.Vector3.Dot(u, v));
            Vector3 w = (1f / m) * System.Numerics.Vector3.Cross(u, v);
            //return new System.Numerics.Vector4(0.5f * m, w.X, w.Y, w.Z);
            return;
        }

        private List<IPAddress> Parse(string delimitedIpAddresses)
        {
            try
            {
                var ipAddressStrings = delimitedIpAddresses.Split(',');
                var ipAddresses = new List<IPAddress>();
                foreach (var ipAddressString in ipAddressStrings)
                {
                    ipAddresses.Add(IPAddress.Parse(ipAddressString));
                }
                return ipAddresses;
            }
            catch (Exception e)
            {
                status += "Unable to parse IP address string: '" + delimitedIpAddresses + "'";
                Console.WriteLine("Exception parsing IP address string...");
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

        // functions
        public static double ComputeXAngle(Microsoft.Kinect.Vector4 q)
        {
            double sinr_cosp = 2 * (q.W * q.X + q.Y * q.Z);
            double cosr_cosp = 1 - 2 * (q.X * q.X + q.Y * q.Y);
            return Math.Atan2(sinr_cosp, cosr_cosp);
        }

        public static double ComputeYAngle(Microsoft.Kinect.Vector4 q)
        {
            float sinp = 2 * (q.W * q.Y - q.Z * q.X);
            if (Math.Abs(sinp) >= 1)
                return Math.PI / 2 * Math.Sign(sinp); // use 90 degrees if out of range
            else
                return Math.Asin(sinp);
        }

        public static double ComputeZAngle(Microsoft.Kinect.Vector4 q)
        {
            float siny_cosp = 2 * (q.W * q.Z + q.X * q.Y);
            float cosy_cosp = 1 - 2 * (q.Y * q.Y + q.Z * q.Z);
            return Math.Atan2(siny_cosp, cosy_cosp);
        }

        public static Vector3 ComputeAngles(System.Numerics.Vector4 quat)
        {
            Microsoft.Kinect.Vector4 q = new Microsoft.Kinect.Vector4();
            q.X = quat.X;
            q.Y = quat.Y;
            q.Z = quat.Z;
            q.W = quat.W;

            return new Vector3((float)ComputeXAngle(q), (float) ComputeYAngle(q),(float) ComputeZAngle(q));
        }

        public static Quaternion FromAngles(Vector3 angles)
        {

            double cy = Math.Cos(angles.Z * 0.5);
            double sy = Math.Sin(angles.Z * 0.5);
            double cp = Math.Cos(angles.Y * 0.5);
            double sp = Math.Sin(angles.Y * 0.5);
            double cr = Math.Cos(angles.X * 0.5);
            double sr = Math.Sin(angles.X * 0.5);

            Quaternion q;
            q.W = (float) (cr * cp * cy + sr * sp * sy);
            q.X = (float) (sr * cp * cy - cr * sp * sy);
            q.Y = (float) (cr * sp * cy + sr * cp * sy);
            q.Z = (float) (cr * cp * sy - sr * sp * cy);

            return q;

        }
    }
}
