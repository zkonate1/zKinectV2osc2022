REM @echo off


set /p targetIP= Enter target IP address (e.g. 192.168.0.255) or hit CR for default [local network broadcast]: 

set /p portNo= Enter port number or hit CR for default [54321]: 

set /p clipDist= Enter distance clip range (meters) or hit CR for default [4.5]: 


IF "%portNo%"=="" (SET portNo=54321)

IF "%clipDist%"=="" (SET clipDist=4.5)


REM PAUSE

IF "%targetIP%"=="" bin\x64\Release\zKinectV2OSC.exe -p %portNo% -d %clipDist%
ELSE  bin\x64\Release\zKinectV2OSC.exe  -i %targetIP% -p %portNo% -d %clipDist%

REM bin\x64\Release\zKinectV2OSC.exe  -i localhost -p 1234 

	


